const express = require("express");
const todosRoutes = require("./src/todos/routes");
const app = express();
const port = 4000;
app.use(express.json());
app.use("/todos", todosRoutes);
app.listen(port, () => console.log(`app listening on port ${port}`));
