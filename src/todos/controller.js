const pool = require("../../db");
const queries = require("./queries");
const getTodos = (req, res) => {
  pool.query(queries.getTodos, (err, result) => {
    if (err) throw err;
    res.status(200).json(result.rows);
  });
};
const getTodosById = (req, res) => {
  const id = parseInt(req.params.id);
  pool.query(queries.getTodosById, [id], (err, result) => {
    if (err) throw err;
    if (result.rows.length === 0) {
      return res.status(404).json({ message: "NOT FOUND" });
    }
    res.status(200).json(result.rows);
  });
};
const addTodo = (req, res) => {
  const { text, isCompleted } = req.body;
  pool.query(queries.checkTodoExist, [text], (err, result) => {
    if (result.rows.length) {
      return res
        .status(400)
        .json({ message: "Already this todo exist not a valid to do" });
    }
    pool.query(queries.addTodo, [text, isCompleted], (err, result) => {
      if (err) throw err;
      res.status(201).send("todo added successfully");
    });
  });
};
const deleteTodo = (req, res) => {
  const id = parseInt(req.params.id);
  pool.query(queries.getTodosById, [id], (err, result) => {
    if (err) throw err;
    if (result.rows.length === 0) {
      return res.status(404).json({ message: "NOT FOUND" });
    }
    pool.query(queries.deleteTodo, [id], (err, result) => {
      if (err) throw err;
      res.status(200).send("todo removed successfully");
    });
  });
};
const updateTodo = (req, res) => {
  const id = parseInt(req.params.id);
  const { isCompleted } = req.body;
  pool.query(queries.getTodosById, [id], (err, result) => {
    if (err) throw err;
    if (result.rows.length === 0) {
      return res.status(400).send({ message: "No such id to update" });
    }
    pool.query(queries.updateTodo, [isCompleted, id], (err, result) => {
      if (err) throw err;
      res.status(200).send("todo updated successfully");
    });
  });
};
module.exports = {
  getTodos,
  getTodosById,
  addTodo,
  deleteTodo,
  updateTodo,
};
