const getTodos = "SELECT * FROM todos";
const getTodosById = "SELECT * FROM todos WHERE id = $1";
const checkTodoExist = "SELECT t FROM todos t WHERE t.text = $1";
const addTodo = "INSERT INTO todos (text, isCompleted) VALUES ($1, $2)";
const updateTodo = "UPDATE todos SET isCompleted = $1 WHERE id = $2 ";
const deleteTodo = "DELETE FROM todos WHERE id = $1";
module.exports = {
  getTodos,
  getTodosById,
  checkTodoExist,
  addTodo,
  deleteTodo,
  updateTodo,
};
