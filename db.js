const Pool = require("pg").Pool;
const pool = new Pool({
  host: "localhost",
  user: "postgres",
  password: "xyz",
  database: "todos",
  port: 5432,
});
module.exports = pool;
